package com.simion.util;

/**
 * Created by dam on 24/02/17.
 */
public class Constantes {

    public static final String _ID = "_id";
    public static final String NOMBRE = "nombre";
    public static final String RAZA = "raza";
    public static final String PESO = "peso";
    public static final String FECHANAC = "fechaNac";

    public static final String APELLIDOS = "apellidos";
    public static final String EMAIL = "email";
    public static final String ANIMAL = "animal";

    public static final String BBDD = "SimionBordean";
    public static final String TB_ANIMAL = "animal";
    public static final String TB_DUENIO = "duenio";
}
