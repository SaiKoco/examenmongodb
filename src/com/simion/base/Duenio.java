package com.simion.base;

import org.bson.types.ObjectId;

/**
 * Created by dam on 24/02/17.
 */
public class Duenio {

    private String nombre;
    private String apellidos;
    private String email;
    private ObjectId animal;


    public Duenio() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ObjectId getAnimal() {
        return animal;
    }

    public void setAnimal(ObjectId animal) {
        this.animal = animal;
    }

    @Override
    public String toString() {
        return nombre + " " + apellidos;
    }
}
