package com.simion.gui;

import com.simion.base.Animal;
import com.simion.base.Duenio;
import com.simion.util.Util;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

/**
 * Created by dam on 24/02/17.
 */
public class Controler implements ActionListener, ListSelectionListener {

    private Ventana view;
    private Modelo modelo;

    public Controler(Ventana view, Modelo modelo) {
        this.view = view;
        this.modelo = modelo;

        modelo.conectar();

        refrescarTabla();
        rellenarCombos();
        addListeners();
    }

    private void rellenarCombos() {
        view.cbRaza.removeAllItems();
        for (String raza:modelo.obtenerRazas()){
            view.cbRaza.addItem(raza);
        }
        view.cbAnimales.removeAllItems();
        for (Animal animal:modelo.obtenerAnimales()){
            view.cbAnimales.addItem(animal);
        }
    }

    private void addListeners() {
        view.guardarButton.addActionListener(this);
        view.eliminarButton.addActionListener(this);
        view.tbTabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsm = view.tbTabla.getSelectionModel();
        lsm.addListSelectionListener(this);
    }

    private void refrescarTabla() {
        view.dtmTabla.setNumRows(0);
        for(Animal animal:modelo.obtenerAnimales()){
            String[] filaAnimal = {
                    animal.getNombre(),
                    animal.getRaza(),
                    String.valueOf(animal.getPeso()),
                    String.valueOf(animal.getFechaNac())
            };
            view.dtmTabla.addRow(filaAnimal);
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String comando = actionEvent.getActionCommand();
        switch (comando){
            case "Guardar":
                String nombreAnimal = view.tfNombre.getText();
                if(nombreAnimal.equals("")){
                    Util.warning("El campo Nombre no puede estar vacio", "Error en el nombre");
                    return;
                }
                else if (view.tfRaza.getText().equals("")){
                    Util.warning("El campo Raza no puede estar vacio.", "Error en la raza");
                    return;
                }
                if (modelo.obtenerAnimal(nombreAnimal) != null){
                    String mensaje = "Ya existe un Animal con el nombre "+nombreAnimal+". Indica otro nombre";
                    String titulo = "Animal ya existe";
                    Util.warning(mensaje, titulo);
                    return;
                }
                Animal unAnimal = new Animal();
                unAnimal.setNombre(nombreAnimal);
                unAnimal.setRaza(view.tfRaza.getText());
                double peso;
                try {
                    peso = Double.parseDouble(view.tfPeso.getText());
                } catch (NumberFormatException nfe){
                    peso = -1;
                }
                unAnimal.setPeso(peso);
                unAnimal.setFechaNac(view.dcFechaNac.getDate());
                modelo.registrar(unAnimal);
                refrescarTabla();
                limpiarGUI();
                break;
            case "Eliminar":
                String razaSeleccionada = (String) view.cbRaza.getSelectedItem();
                for (Animal animal:modelo.obtenerAnimales()){
                    if (animal.getRaza().equals(razaSeleccionada)){
                        modelo.elminar(animal);
                    }
                }
                view.cbRaza.removeItem(razaSeleccionada);
                refrescarTabla();
                break;
            case "Modificar":
                view.guardarButton.doClick();
                break;
            case "Guardar Dueño":
                Duenio duenio = new Duenio();
                duenio.setNombre(view.tfDuenio.getText());
                duenio.setApellidos(view.tfApellidos.getText());
                duenio.setEmail(view.tfEmail.getText());
                duenio.setAnimal((ObjectId) view.cbAnimales.getSelectedItem());
                modelo.registrarDuenio(duenio);
                break;
            default:
                break;
        }
    }

    private void limpiarGUI() {
        view.tfNombre.setText("");
        view.tfRaza.setText("");
        view.tfPeso.setText("");
        view.dcFechaNac.setDate(null);
    }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        System.out.println(view.tbTabla.getColumnName(0));
        String seleccion = (String) view.tbTabla.getValueAt(view.tbTabla.getSelectedRow(), 0);
        Animal animal = modelo.obtenerAnimal(seleccion);
        view.tfNombre.setText(animal.getNombre());
        view.tfRaza.setText(animal.getRaza());
        view.tfPeso.setText(String.valueOf(animal.getPeso()));
        view.dcFechaNac.setDate(animal.getFechaNac());
    }
}
