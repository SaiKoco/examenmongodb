package com.simion.base;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by dam on 24/02/17.
 */
public class Animal {

    private org.bson.types.ObjectId id;
    private String nombre;
    private String raza;
    private Double peso;
    private Date fechaNac;

    public Animal() {

    }

    public org.bson.types.ObjectId getId() {
        return id;
    }

    public void setId(org.bson.types.ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
