package com.simion.gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by dam on 24/02/17.
 */
public class Ventana {
     JPanel panel1;
     JTextField tfNombre;
     JTextField tfRaza;
     JTextField tfPeso;
     JTable tbTabla;
     JDateChooser dcFechaNac;
     JButton guardarButton;
     JButton eliminarButton;
     JComboBox cbRaza;
     JButton modificarButton;
     JTextField tfDuenio;
     JTextField tfApellidos;
     JTextField tfEmail;
     JComboBox cbAnimales;
     JButton guardarDueñoButton;
     JList lsDuenios;
    DefaultTableModel dtmTabla;

    public Ventana(){
        JFrame frame = new JFrame("Examen Simion Bordean");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtmTabla = new DefaultTableModel();
        dtmTabla.addColumn("Nombre");
        dtmTabla.addColumn("Raza");
        dtmTabla.addColumn("Peso");
        dtmTabla.addColumn("Fecha Nacimiento");
        tbTabla.setModel(dtmTabla);
    }
}
