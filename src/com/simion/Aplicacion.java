package com.simion;

import com.simion.gui.Controler;
import com.simion.gui.Modelo;
import com.simion.gui.Ventana;

/**
 * Created by dam on 24/02/17.
 */
public class Aplicacion {

    public static void main(String[] args){
        Ventana view = new Ventana();
        Modelo modelo = new Modelo();
        Controler c = new Controler(view, modelo);

    }
}
