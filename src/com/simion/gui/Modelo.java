package com.simion.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.simion.base.Animal;
import com.simion.base.Duenio;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static com.simion.util.Constantes.*;

/**
 * Created by dam on 24/02/17.
 */
public class Modelo {

    private MongoClient client;
    private MongoDatabase db;

    public Modelo() {

    }

    public void conectar(){
        client = new MongoClient();
        db = client.getDatabase(BBDD);
    }

    public void desconectar(){
        client.close();
    }

    public Collection<Animal> obtenerAnimales() {
        ArrayList<Animal> animales = new ArrayList<>();
        FindIterable findIterable = db.getCollection(TB_ANIMAL).find();
        Iterator<Document> iterator = findIterable.iterator();
        while (iterator.hasNext()){
            Document document = iterator.next();
            Animal animal = new Animal();
            animal.setId(document.getObjectId(_ID));
            animal.setNombre(document.getString(NOMBRE));
            animal.setRaza(document.getString(RAZA));
            animal.setPeso(document.getDouble(PESO));
            animal.setFechaNac(document.getDate(FECHANAC));
            animales.add(animal);
        }

        return animales;
    }

    public void registrar(Animal unAnimal) {
        Document document = new Document()
                .append(NOMBRE, unAnimal.getNombre())
                .append(RAZA, unAnimal.getRaza())
                .append(PESO, unAnimal.getPeso())
                .append(FECHANAC, unAnimal.getFechaNac());
        db.getCollection(TB_ANIMAL).insertOne(document);
    }

    public void elminar(Animal elAnimal){
        Document document = new Document()
                .append(NOMBRE, elAnimal.getNombre())
                .append(RAZA, elAnimal.getRaza())
                .append(PESO, elAnimal.getPeso())
                .append(FECHANAC, elAnimal.getFechaNac());
        db.getCollection(TB_ANIMAL).deleteOne(document);
    }

    public Animal obtenerAnimal(String nombreAnimal) {
        Document condicion = new Document(NOMBRE, nombreAnimal);
        FindIterable findIterable = db.getCollection(TB_ANIMAL).find(condicion);
        Iterator<Document> iterator = findIterable.iterator();
        if (iterator.hasNext()){
            Document document = iterator.next();
            Animal animal = new Animal();
            animal.setId(document.getObjectId(_ID));
            animal.setNombre(document.getString(NOMBRE));
            animal.setRaza(document.getString(RAZA));
            animal.setPeso(document.getDouble(PESO));
            animal.setFechaNac(document.getDate(FECHANAC));
            return animal;
        }

        return null;
    }

    public Collection<String> obtenerRazas() {
        ArrayList<String > razas = new ArrayList<>();
        FindIterable iterable = db.getCollection(TB_ANIMAL).find();
        Iterator<Document> iterator = iterable.iterator();
        while (iterator.hasNext()){
            Document document = iterator.next();
            String raza = document.getString(RAZA);
            if (!razas.contains(raza)){
                razas.add(raza);
            }
        }
        return razas;
    }

    public void registrarDuenio(Duenio duenio) {
        Document document = new Document()
                .append(NOMBRE, duenio.getNombre())
                .append(APELLIDOS, duenio.getApellidos())
                .append(EMAIL, duenio.getEmail())
                .append(ANIMAL, duenio.getAnimal());
        db.getCollection(TB_DUENIO).insertOne(document);

    }
}
